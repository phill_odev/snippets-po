# Changelog

## Changes

- Ensure that dependencies have no major vulnerabilities accepting and add step to pipeline to prevent code being accepted that has high criticality vulnerabilities
- Render comments in a div below the snippets
- Implement upvote functionality allowing user to upvote as many times as possible a snippet with immediate re-render of the snippets count.
- Fix bug where updating a snippet would push unneccessary data into store
- Refactor PUT method to more accurate PATCH method for updating snippet.
- Add visual indicators when there is something being loaded on the page
- Add error pages for each of the scenarios where errors can be thrown on the UI

## Future improvements

- Could remove the current inefficient GET call to the backend to get the snippets which is then partially duplicated in the individual GET calls per snippet for additional detail
- Improve user experience of the application
    - Lighthouse has shown accessibility issues with insufficient contrast between elements
- Multiple clients calling the upvote API at the same time will overwrite each others counter (2 upvotes results in +1 to votes cound). 
    - This could be resolved via queuing the updates on the backend and not allowing the client to directly overwrite this field. 
    - Of course this would require not using JSON server to serve the backend
- Improve test coverage