// Based on https://usehooks.com/usePrevious/, but will not update the stored value
// if the new value is nil

import { useEffect, useRef } from "react";

export function usePreviousValidValue(value) {
    const ref = useRef();
    // Store current value in ref iff not nil
    useEffect(() => {
        if (value) {
            ref.current = value;
        }
    }, [value]);

    return ref.current;
}
