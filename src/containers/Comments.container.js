import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import Comment from "../components/Comment";
import LoadingIndicator from "../components/LoadingIndicator";

function CommentsContainer({ snippetId }) {
    const [makeRequest, { data, error, loading }] = useFetch();
    const [endpoint] = useState(
        `/comments/?snippetId=${snippetId}&_expand=author`
    );

    useEffect(() => makeRequest(endpoint), [makeRequest, endpoint]);

    if (loading) {
        return <LoadingIndicator size="small" />
    }

    if (error) {
        return <div>Failed to get data for snippet {snippetId}</div>
    }

    if (data) {
        return (
            <>
                {data.length === 0 ?
                    <NoComments /> :
                    <Comments comments={data}
                    />}
            </>
        )
    }

    return null;
}

function NoComments() {
    return <h5 className="mdc-typography--body1">No comments left</h5>
}

function Comments({ comments }) {
    return comments.map(comment => <Comment comment={comment} key={comment.id} />)
}

export default CommentsContainer;
