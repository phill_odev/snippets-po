import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import ErrorPage from "../components/ErrorPage";
import LoadingIndicator from "../components/LoadingIndicator";
import SnippetCardContainer from "./SnippetCard.container";
import styles from "./Snippets.module.css";

function SnippetsContainer() {
    const [makeRequest, { loading, data, error }] = useFetch();

    const [endpoint] = useState("/snippets?_sort=votes&_order=desc&fields=votes");

    // Initial fetch
    useEffect(() => {
        makeRequest(endpoint);
    }, [makeRequest, endpoint]);

    if (loading) {
        return <LoadingIndicator size={"large"} />
    }

    if (error) {
        return <ErrorPage error={error} />;
    }

    if (data) {
        return (
            <section className={styles.Container}>
                {data.map((snippet) => (
                    <SnippetCardContainer snippetId={snippet.id} key={snippet.id} />
                ))}
            </section>
        );
    }

    return null;
}

export default SnippetsContainer;
