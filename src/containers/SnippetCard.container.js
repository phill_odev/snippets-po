import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import LoadingIndicator from "../components/LoadingIndicator";
import SnippetCard from "../components/SnippetCard";
import CommentsContainer from "./Comments.container";

function SnippetCardContainer({ snippetId }) {
    const [makeRequest, { data, error, loading, setData }] = useFetch();

    // See https://github.com/typicode/json-server#relationships
    // This expands the author and language objects so we
    // don't have to fetch them by id subsequently
    const [endpoint] = useState(
        `/snippets/${snippetId}/?_expand=author&_expand=language`
    );

    useEffect(() => makeRequest(endpoint), [makeRequest, endpoint]);

    if (loading) {
        return <LoadingIndicator size="small" />
    }

    if (error) {
        return <div>Failed to get data for snippet {snippetId}</div>
    }

    if (data) {
        return (
            <SnippetCard snippetData={data} updateSnippetData={setData}>
                <CommentsContainer snippetId={data.id} />
            </SnippetCard>
        )
    }

    return null;
}

export default SnippetCardContainer;
