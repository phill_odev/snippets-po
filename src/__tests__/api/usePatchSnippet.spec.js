import axios from "axios";
import { usePatchSnippet } from "../../api/usePatchSnippet.hook";
import { jest } from '@jest/globals'
import { renderHook, act } from '@testing-library/react-hooks';
import { waitFor } from "@testing-library/react";

jest.mock("axios")

describe('useFetchHook', () => {
    it("should initialise with loading set to false without any error or data stored", () => {
        const { result } = renderHook(() => usePatchSnippet());
        const [_, { loading, data, error }] = result.current;

        expect(loading).toBe(false);
        expect(data).toBeNull();
        expect(error).toBeNull()
    })

    it("should add returned data from axios call to data variable", async () => {
        axios.patch.mockImplementation(() => Promise.resolve({ data: {} }))
        const { result } = renderHook(() => usePatchSnippet());
        const [patch] = result.current;

        await act(async () => { await patch() })

        await waitFor(() => {
            const [_, { loading, data, error }] = result.current;
            expect(loading).toBe(false);
            expect(data).toEqual({});
            expect(error).toBeNull()
        })
    })

    it("should capture the error from a failed axios call to the error variable variable", async () => {
        axios.patch.mockImplementation(() => Promise.reject({ message: "failed request" }))
        const { result } = renderHook(() => usePatchSnippet());
        const [patch] = result.current;

        await act(async () => { await patch() })

        await waitFor(() => {
            const [_, { loading, data, error }] = result.current;
            expect(loading).toBe(false);
            expect(data).toBeNull();
            expect(error).toEqual({ message: "failed request" })
        })
    })
})