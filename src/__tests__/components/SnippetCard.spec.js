import React from 'react';
import renderer from 'react-test-renderer';
import SnippetCard from '../../components/SnippetCard';
import Enzyme, { mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import * as hook from "../../api/usePatchSnippet.hook";

Enzyme.configure({ adapter: new Adapter() })

const snippet = {
    "language": "Go",
    "author": "tim",
    "title": "An example React component",
    "description": "Bleh",
    "body": "dGVzdA==",
    "votes": 3,
    "id": 1
}

const mockPatch = jest.fn();
jest.mock("../../api/usePatchSnippet.hook", () => ({
    usePatchSnippet: jest.fn().mockReturnValue(undefined)
}))

describe("SnippetCard", () => {

    beforeEach(() => {
        mockPatch.mockClear()
    })
    it('should render correctly and match existing snapshot', () => {
        jest.spyOn(hook, 'usePatchSnippet')
            .mockReturnValue([mockPatch, { loading: false, error: undefined }]);

        const component = renderer.create(
            <SnippetCard snippetData={snippet} />
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('patch function from usePatchSnippet hook to be called when the button is clicked', () => {
        jest.spyOn(hook, 'usePatchSnippet')
            .mockReturnValue([mockPatch, { loading: false, error: undefined }]);

        const component = mount(<SnippetCard snippetData={snippet} />);
        const button = component.find('IconTextButton')
        button.simulate('click');
        expect(mockPatch).toBeCalledTimes(1)
    });
})
