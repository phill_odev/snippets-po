// Hook to update a snippet by id

import { useCallback, useState } from "react";
import axios from "axios";

export function usePatchSnippet(baseUrl = "/api") {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  async function patch({ snippetId, body, onPatched = () => {} } = {}) {
    const url = `${baseUrl}/snippets/${snippetId}` ;
    setLoading(true);
    setData(null);
    setError(null);
    await axios
      .patch(url, body)
      .then((res) => {
        setData(res.data);
        onPatched(res.data);
      })
      .catch(setError)
      .finally(() => {
        setLoading(false);
      });
  }

  return [useCallback(patch, [baseUrl]), { loading, data, error }];
}
