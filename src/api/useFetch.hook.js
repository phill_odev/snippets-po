// Hook to fetch data from the backend

import { useCallback, useState } from "react";
import axios from "axios";

export function useFetch(baseUrl = "/api") {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  function makeRequest(endpoint) {
    const url = baseUrl + endpoint;
    setLoading(true);
    setData(null);
    setError(null);
    axios
      .get(url)
      .then((res) => {
        setData(res.data);
      })
      .catch(setError)
      .finally(() => setLoading(false));
  }

  return [useCallback(makeRequest, [baseUrl]), { loading, data, error, setData }];
}
