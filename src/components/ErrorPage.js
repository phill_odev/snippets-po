import styles from "./SnippetCard.module.css";

function ErrorPage(props) {
  const { error } = props;

  return (
    <div className={styles.Card}>
        <h2 className="mdc-typography--headline2">Fatal error in snippets app</h2>
        <h4 className="mdc-typography--headline4"><b>Error Message:</b> {error.message}</h4>
    </div>
  );
}

export default ErrorPage;
