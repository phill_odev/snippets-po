// Simple icon text button with Mono icons
// https://icons.mono.company/

import style from "./IconTextButton.module.css";

const IconTextButton = ({ icon, onClick, disabled, children, ...props }) => {
  return (
    <button
      disabled={disabled}
      className={style.Button}
      onClick={onClick}
      {...props}
    >
      {children} <i className={`mi-${icon}`} />
    </button>
  );
};

export default IconTextButton;
