import styles from "./LoadingIndicator.module.css";
import CircularProgress from '@mui/material/CircularProgress';

const indicatorSizes = {
    xsmall: 20,
    small: 40,
    large: 400
};

const LoadingIndicator = (props) => {
    const {
        size = "small"
    } = props;

    return (
        <div className={styles.Container}>
            <CircularProgress
                className={styles.Indicator}
                size={indicatorSizes[size] || indicatorSizes.small}
            />
        </div>
    );
};

export default LoadingIndicator;
