import styles from "./Comment.module.css"
import { Avatar } from "@mui/material";

function Comment(props) {
	const { comment } = props;

	return (
		<div className={styles.Comment}>
			<Avatar {...nameToAvatarProps(comment.author.name)} /> <p className="mdc-typography--body2">{comment.body}</p>
		</div>
	);
}

function nameToAvatarProps(name) {
	return {
		alt: name,
		children: name[0],
		sx: {
			bgcolor: "#f00"
		}
	}
}

export default Comment;
