import { Buffer } from "buffer";
import { useCallback } from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import { atomOneDark } from "react-syntax-highlighter/dist/cjs/styles/hljs";
import { usePatchSnippet } from "../api/usePatchSnippet.hook";

import IconTextButton from "./IconTextButton";
import LanguageBadge from "./LanguageBadge";
import LoadingIndicator from "./LoadingIndicator";
import styles from "./SnippetCard.module.css";

// Map language keys to their SyntaxHighlighter styles
// See https://github.com/react-syntax-highlighter/react-syntax-highlighter
const LanguageStyleMap = {
    js: "javascript",
    go: "go",
    py: "python",
};

// Try and decode the snippet from base64
function tryDecodeSnippet(raw) {
    try {
        return { decoded: Buffer.from(raw, "base64").toString(), err: null };
    } catch (err) {
        return { err: `Failed to parse Base64 encoded snippet: ${err.message}` };
    }
}

function SnippetCard(props) {
    const { snippetData, updateSnippetData, children } = props
    const { id, language, author, title, body, description, votes } = snippetData;
    const [patch, { loading, error }] = usePatchSnippet();

    const incrementUpvote = useCallback(() => {
        patch({
            snippetId: id,
            body: {
                votes: snippetData.votes + 1
            },
            onPatched: (patchedData) => {
                updateSnippetData({
                    ...snippetData,
                    ...patchedData
                })
            }
        })
    }, [snippetData, id, patch, updateSnippetData]);

    // Default to plaintext if no supported language is supplied
    const syntax = LanguageStyleMap[language.key] || "plaintext";

    if (error) {
        console.error(error);
        return <div className={styles.Card}>
            <LanguageBadge language={language} />
            <header>
                <h2 className="mdc-typography--headline4">Unable to get data for snippet</h2>
            </header>
        </div>;
    }

    const { decoded, decodedDataError } = tryDecodeSnippet(body);

    return (
        <div className={styles.Card}>
            <LanguageBadge language={language} />
            <header>
                <h2 className="mdc-typography--headline4">{title}</h2>
                <IconTextButton icon="arrow-up" onClick={incrementUpvote} disabled={loading}>
                    {loading ? <LoadingIndicator size="xsmall" /> : "Upvote"}
                </IconTextButton>
            </header>
            <p className="mdc-typography--subtitle2">By {author.name}</p>
            <p className="mdc-typography--body2">Votes: {votes}</p>
            <p className="mdc-typography--body2">{description}</p>
            <SyntaxHighlighter language={syntax} style={atomOneDark}>
                {decodedDataError || decoded}
            </SyntaxHighlighter>
            {children}
        </div>
    );
}

export default SnippetCard;
